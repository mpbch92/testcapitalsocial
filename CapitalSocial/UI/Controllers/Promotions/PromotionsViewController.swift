//
//  PromotionsViewController.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 04/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import UIKit

class PromotionsViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var promoSelected:promotionCell? = nil
    let promoContent:[promotionCell] = [
        promotionCell(image: UIImage(named: "PromoZonaFitness") , name: "Zona Fitness"),
        promotionCell(image: UIImage(named: "PromoWingstop"), name: "Wing Stop"),
        promotionCell(image: UIImage(named: "PromoTizoncito"), name: "El Tizoncito"),
        promotionCell(image: UIImage(named: "PromoItaliannis"), name: "Italiannis"),
        promotionCell(image: UIImage(named: "PromoIdea"), name: "Idea"),
        promotionCell(image: UIImage(named: "PromoCinepolis"), name: "Cinepolis"),
        promotionCell(image: UIImage(named: "PromoChilis"), name: "Chilis"),
        promotionCell(image: UIImage(named: "PromoBurguerKing") , name: "Burger King"),
        promotionCell(image: UIImage(named: "PromoBenavides"), name: "Benavides")
        
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCollection()
        setupNavigation()
    }
    
    func setupNavigation(){
        navigationController?.navigationBar.topItem?.title = "Capital Social"
        
    }
    func setUpCollection(){
        
        collectionView.dataSource = self
        collectionView.delegate = self
        self.collectionView.register(UINib(nibName: "PromotionsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DetailViewController{
            vc.promoSelected = self.promoSelected
        }
    }

//MARK: Collection View
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return promoContent.count
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? PromotionsCollectionViewCell else{ return UICollectionViewCell()}
    cell.configure(with: promoContent[indexPath.row])
    return cell
}

func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    self.promoSelected = promoContent[indexPath.row]
    performSegue(withIdentifier: "promoDetail", sender: nil)
    
}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let yourWidth = collectionView.bounds.width/2
    let yourHeight = yourWidth
    
    return CGSize(width: yourWidth, height: yourHeight)
}

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 20
}
    



}


