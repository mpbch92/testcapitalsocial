//
//  PromotionsCollectionViewCell.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 04/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import UIKit

class PromotionsCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var promoImage: UIImageView!
    @IBOutlet weak var promoLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.greenBG
    }
    
    public func configure(with model: promotionCell) {
        
        if let imageToLoad = model.image {
          promoImage.image = imageToLoad
        }
          promoLabel.text = model.name
      }

}
