//
//  DetailViewController.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 05/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var promoLabel: UILabel!
    @IBOutlet weak var promoImage: UIImageView!
    var promoSelected:promotionCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.promoLabel.text = promoSelected?.name
        self.promoImage.image = promoSelected?.image
        let add = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(btnShare_clicked))
        let map = UIBarButtonItem(title: "Map", style: .plain, target: self, action: #selector(btnLocation_clicked))
        let cerrar = UIBarButtonItem(title: "Cerrar sesión,", style: .plain, target: self, action: #selector(logOut))
        navigationItem.rightBarButtonItems = [add, map , cerrar]
    }
    
    @objc func btnShare_clicked() {
       let items:[Any] = [promoSelected?.name ,promoSelected?.image ]
       let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
       present(ac, animated: true)
    }
    
    @objc func btnLocation_clicked() {
          performSegue(withIdentifier: "mapSegue", sender: nil)
       }
    
    
    @objc func logOut() {
        LocalAPI.loggedIn = false
        self.dismiss(animated: true, completion: nil)
       }
    
}
