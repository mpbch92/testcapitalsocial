//
//  LoginViewController.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 02/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import UIKit
import QRCodeReader

class LoginViewController: UIViewController, UITextFieldDelegate {
    // MARK: IBOutlets
    @IBOutlet weak var loginOutlet: UIButton!
    @IBOutlet weak var qrOutlet: UIButton!
    @IBOutlet weak var registerOutlet: UIButton!
    @IBOutlet weak var recoverPaswordOutlet: UIButton!
    @IBOutlet weak var scanOutlet: UIButton!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: QRCOdeREader
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            
            // Configure the view controller (optional)
            $0.showTorchButton        = false
            $0.showSwitchCameraButton = false
            $0.showCancelButton       = false
            $0.showOverlayView        = true
            $0.rectOfInterest         = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView(){
        self.activityIndicator.isHidden = true
        //UI colors
        loginOutlet.backgroundColor = UIColor.lightPink
        qrOutlet.backgroundColor = UIColor.lightPink
        registerOutlet.layer.borderWidth = 1
        registerOutlet.setTitleColor(UIColor.lightPink, for: .normal)
        registerOutlet.layer.borderColor = UIColor.lightPink.cgColor
        
        //TextField delegates
        userTextField.delegate = self
        PasswordTextField.delegate = self
        userTextField.tag = 0
        
    }
    
    // MARK: IBActions
    
    @IBAction func loginAction(_ sender: Any) {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        let service = LoginService()
        //service.login(user: "testnaat@na-at.com.mx", password: "56Forj2018")
        service.login(user: userTextField.text ?? "", password: PasswordTextField.text ?? "")
        {
            
            (response:LoginResponseModel? , error:Error?) in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            if error == nil && !(response?.access_token.isEmpty ?? true){
                self.userTextField.text = ""
                self.PasswordTextField.text = ""
                self.successLogin()
            }
            else{
                let alert = UIAlertController(title: "Error", message: "Usuario y/o Contraseña incorrecta", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Cerrar", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
    
    @IBAction func scanAction(_ sender: Any) {
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if result != nil{
                self.successLogin()
            }  else{
                let alert = UIAlertController(title: "Error", message: "Código QR no válido", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Cerrar", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
        readerVC.modalPresentationStyle = .formSheet
        present(readerVC, animated: true, completion: nil)
    }
    
    func successLogin(){
        LocalAPI.loggedIn = true
        
        let promoVC = self.storyboard!.instantiateViewController(withIdentifier: "promoVC") as! PromotionsViewController
        let navController = UINavigationController(rootViewController: promoVC)
        navController.modalPresentationStyle = .overCurrentContext
        self.present(navController, animated:true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userTextField {
            PasswordTextField.becomeFirstResponder()
        }
        else{
            PasswordTextField.resignFirstResponder()
        }
        return false
    }
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
}
