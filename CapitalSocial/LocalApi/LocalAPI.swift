//
//  LocalAPI.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 04/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import Foundation
import Foundation
struct LocalAPI
{
    static let LOGGED_IN_KEY   = "isLoggedIn"

    static var loggedIn: Bool {
        get {
            if let loggedIn = UserDefaults.standard.value(forKey: LocalAPI.LOGGED_IN_KEY)
            {
                if let _loggedIn = loggedIn as? Bool
                {
                    return _loggedIn
                }
                else
                {
                    return false
                }
            }
            else
            {
                return false
            }
        }
        set
        {
            UserDefaults.standard.set(newValue, forKey: LocalAPI.LOGGED_IN_KEY)
        }
    }
}

