//
//  LoginService.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 03/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import Foundation
import Alamofire

class LoginService: Api{
    func login(user:String , password:String, callback: @escaping(LoginResponseModel? , Error?) ->Void ){

        
        let shaPassword = passwordToSha256(password: password)
        let URL = self.urlConMetodo(method: "")
        let basicHeader = "Basic Wm1Ga0xXTXlZeTF3YjNKMFlXdz06TWpoa04yUTNNbUppWVRWbVpHTTBObVl4Wmpka1lXSmpZbVEyTmpBMVpEVXpaVFZoT1dNMVpHVTROakF4TldVeE9EWmtaV0ZpTnpNd1lUUm1ZelV5WWc9PQ=="
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization":  basicHeader
        ]
        let params = [
            "grant_type":  "password",
            "username":     user,
            "password":     "a0700af71a183b82aa4d79682475b151161bf91138d77f6f10937240f40814bd" //shaPAssword here.
            
        ] as [String : Any]

        AF.request(URL, method: .post, parameters: params, headers: headers).response  { response in
            switch response.result {
                   case .success(let value):
                    let jsonString = (String(data: value!, encoding: .utf8)!)
                    let jsonData = jsonString.data(using: .utf8)!
                    let loginResponse = try? JSONDecoder().decode(LoginResponseModel.self, from: jsonData)
                    callback(loginResponse , nil)
                   case .failure(let error):
                       print(error)
                       callback(nil, error)
            }
        }
    }

   private func passwordToSha256(password:String) -> String{
        return password.data(using: .ascii)!.sha256
    }
}

