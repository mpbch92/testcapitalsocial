//
//  APO.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 04/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import Foundation

class Api {
    var gateway: String = ""
    
    init(){
        self.gateway = Config().gateway
    }
    
    func urlConMetodo(method: String) -> String {
        return self.gateway + method
    }
}

