//
//  ColorExtension.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 02/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import UIKit
extension UIColor {
    
    // Setup custom colours we can use throughout the app using hex values
    static let lightPink = UIColor(hex: 0xFF149B)
    static let darkPink = UIColor(hex: 0xFF149B)
    static let grey = UIColor(hex: 0xCCCCCC)
    static let darkGrey = UIColor(hex: 0x424242)
    static let greenBG = UIColor(hex: 0xC4CFCF)
    
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    

    convenience init(hex: Int, a: CGFloat = 1.0) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF,
            a: a
        )
    }
}
