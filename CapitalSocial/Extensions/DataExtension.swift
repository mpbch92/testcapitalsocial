//
//  DataExtension.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 04/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import Foundation
import CryptoKit
import CommonCrypto

private func hexString(_ iterator: Array<UInt8>.Iterator) -> String {
    return iterator.map { String(format: "%02x", $0) }.joined()
}
extension Data {
    public var sha256: String {
        if #available(iOS 13.0, *) {
            return hexString(SHA256.hash(data: self).makeIterator())
        } else {
            var digest = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
            self.withUnsafeBytes { bytes in
                _ = CC_SHA256(bytes.baseAddress, CC_LONG(self.count), &digest)
            }
            return hexString(digest.makeIterator())
        }
    }

}
