//
//  promotionCell.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 04/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import UIKit

struct promotionCell {
    let image: UIImage?
    let name: String
}
