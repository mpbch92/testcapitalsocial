//
//  responseModel.swift
//  CapitalSocial
//
//  Created by Miguel Perera on 04/04/20.
//  Copyright © 2020 Miguel Perera. All rights reserved.
//

import Foundation


class LoginResponseModel: Decodable {

    var access_token = ""
    var token_type = ""
    var refresh_token = ""
    var expires_in = 0
    var scope = ""
    var jti = ""
}
